from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('user', views.user, name='user'),
    path('nutrition', views.nutrition, name='nutrition'),
    path('exercise', views.exercise, name='exercise'),
    path('goal', views.goal, name='goal'),
    path('group', views.group, name='group'),
    path('log-in-user', views.log_in_user, name='log_in_user'),
    path('log-out-user', views.log_out_user, name='log_out_user'),
    path('new-user', views.register_user, name='register_user'),
]

from django.shortcuts import render
from .models import *
from .forms import *
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.contrib.auth import logout as auth_logout
from django.contrib.auth import login as auth_login
from datetime import datetime, date


# Create your views here.


@login_required(login_url='log_in_user')
def index(request):
    logged_in_user = request.user
    meal_list = Meal.objects.all()
    exercise_list = Exercise.objects.all()

    today = date.today().day

    today_meal_list = []
    today_exercise_list = []

    for meal in meal_list:
        if meal.date_time_added.day == today:
            today_meal_list.append(meal)

    for exercise in exercise_list:
        if exercise.date_time_added.day == today:
            today_exercise_list.append(exercise)

    context = {'today_meal_list': today_meal_list, 'today_exercise_list': today_exercise_list, 'logged_in_user': logged_in_user}

    return render(request, "index.html", context)


@login_required(login_url='log_in_user')
def user(request):
    logged_in_user = request.user

    if request.method == 'POST':
        filled_new_weight = UpdateWeight(request.POST)
        filled_new_height = UpdateHeight(request.POST)
        filled_new_email = UpdateEmail(request.POST)
        filled_new_password = UpdatePassword(request.POST)

        if filled_new_weight.is_valid():
            logged_in_user.profile.weight_kg = filled_new_weight.cleaned_data['new_weight']

            weight_kg = filled_new_weight.cleaned_data['new_weight']
            height_m = logged_in_user.profile.height_m

            logged_in_user.profile.bmi = float(weight_kg / (height_m * height_m))
            logged_in_user.profile.save()

        if filled_new_height.is_valid():
            logged_in_user.profile.height_m = filled_new_height.cleaned_data['new_height']

            weight_kg = logged_in_user.profile.weight_kg
            height_m = filled_new_height.cleaned_data['new_height']

            logged_in_user.profile.bmi = float(weight_kg / (height_m * height_m))
            logged_in_user.profile.save()

        if filled_new_email.is_valid():
            logged_in_user.email = filled_new_email.cleaned_data['new_email']
            logged_in_user.save()

        if filled_new_password.is_valid():
            logged_in_user.password = filled_new_password.cleaned_data['new_password']
            logged_in_user.save()

    new_weight_form = UpdateWeight()
    new_height_form = UpdateHeight()
    new_email_form = UpdateEmail()
    new_password_form = UpdatePassword()
    context = {'logged_in_user': logged_in_user,
               'new_weight_form': new_weight_form,
               'new_height_form': new_height_form,
               'new_email_form': new_email_form,
               'new_password_form': new_password_form}

    return render(request, "user.html", context)


@login_required(login_url='log_in_user')
def nutrition(request):
    logged_in_user = request.user
    meal_list = logged_in_user.profile.meals.all()
    context = {'meal_list': meal_list}

    return render(request, "nutrition.html", context)


@login_required(login_url='log_in_user')
def exercise(request):
    logged_in_user = request.user
    exercise_list = logged_in_user.profile.exercises.all()
    exercise_form = CreateExercise()
    context = {'exercise_list': exercise_list, 'exercise_form': exercise_form}

    return render(request, "exercise.html", context)


@login_required(login_url='log_in_user')
def goal(request):
    # get logged in user from session, and use it to get related goals
    logged_in_user = request.user
    goal_list = logged_in_user.profile.goals.all()
    create_goal_form = CreateGoal()

    context = {'goal_list': goal_list, 'create_goal_form': create_goal_form}

    return render(request, "goal.html", context)


@login_required(login_url='log_in_user')
def group(request):
    logged_in_user = request.user
    group_list = logged_in_user.profile.groups.all()

    context = {'group_list': group_list}

    return render(request, "group.html", context)


def log_in_user(request):
    try:
        if request.method == 'POST':
            filled_sign_in_form = SignInUser(request.POST)

            if filled_sign_in_form.is_valid():
                username = filled_sign_in_form.cleaned_data['username']
                password = filled_sign_in_form.cleaned_data['password']
                user = User.objects.get(username=username, password=password)

                if user is not None:
                    print('Login success!')
                    auth_login(request, user)
                    return redirect('/')
                else:
                    return redirect('log_in_user')

        sign_in_form = SignInUser()

        context = {'sign_in_form': sign_in_form, 'sign_in_error': False}

        return render(request, "login.html", context)

    except Exception as e:
        sign_in_form = SignInUser()
        context = {'sign_in_form': sign_in_form, 'sign_in_error': e}
        print(context)

        return render(request, "login.html", context)


@login_required(login_url='log_in_user')
def log_out_user(request):
    auth_logout(request)

    return redirect('log_in_user')


def register_user(request):
    try:
        if request.method == 'POST':
            filled_user_form = RegisterUser(request.POST)
            filled_profile_form = RegisterProfile(request.POST)

            if filled_user_form.is_valid() and filled_profile_form.is_valid():
                user = filled_user_form.save()
                profile = filled_profile_form.save(commit=False)
                weight_kg = filled_profile_form.cleaned_data['weight_kg']
                height_m = filled_profile_form.cleaned_data['height_m']
                profile.bmi = float(weight_kg / (height_m * height_m))
                profile.user = User.objects.get(pk=user.pk)
                profile.save()

                auth_login(request, user)
                return redirect('/')

        user_form = RegisterUser()
        profile_form = RegisterProfile()

        context = {'user_form': user_form, 'profile_form': profile_form, 'register_error': False}

        return render(request, "new-user.html", context)

    except Exception as e:
        user_form = RegisterUser()
        profile_form = RegisterProfile()

        context = {'user_form': user_form, 'profile_form': profile_form, 'register_error': e}

        return render(request, "new-user.html", context)



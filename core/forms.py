from django import forms
from .models import *
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit

# django form declarations go here


class DateInput(forms.DateInput):
    input_type = 'date'


class RegisterUser(forms.ModelForm):
    """ form for registering a django user object, containing authentication info """
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password')

    # override password form field to pass PasswordInput widget
    password = forms.CharField(label='Password', widget=forms.PasswordInput)

    helper = FormHelper()
    helper.form_tag = False  # form tag not instantiated as user form is linked with profile form


class RegisterProfile(forms.ModelForm):
    """ additional form items declared not related to authentication """
    class Meta:
        model = Profile
        fields = ('weight_kg', 'height_m', 'gender', 'age')

    helper = FormHelper()
    helper.add_input(Submit('submit', 'Submit', css_class='btn btn-primary'))
    helper.form_tag = False  # form tag not instantiated as profile form is linked with user form


class UpdateWeight(forms.Form):
    """ form to update user profile's weight """
    new_weight = forms.FloatField(label='New Weight')

    helper = FormHelper()
    helper.add_input(Submit('submit', 'Submit', css_class='btn btn-outline-primary long-button', css_id='goal-button'))


class UpdateHeight(forms.Form):
    """ form to update user profile's height """
    new_height = forms.FloatField(label='New Height')

    helper = FormHelper()
    helper.add_input(Submit('submit', 'Submit', css_class='btn btn-outline-primary long-button', css_id='goal-button'))


class UpdateEmail(forms.Form):
    """ form to update user's email address """
    new_email = forms.CharField(label='New Email')

    helper = FormHelper()
    helper.add_input(Submit('submit', 'Submit', css_class='btn btn-outline-primary long-button', css_id='goal-button'))


class UpdatePassword(forms.Form):
    """ form to update user's password """
    new_password = forms.CharField(label='New Password', widget=forms.PasswordInput)

    helper = FormHelper()
    helper.add_input(Submit('submit', 'Submit', css_class='btn btn-outline-primary long-button', css_id='goal-button'))


class SignInUser(forms.Form):
    """ sign in form for users to access their account """
    username = forms.CharField(label='Username')
    password = forms.CharField(label='Password', widget=forms.PasswordInput)

    helper = FormHelper()
    helper.add_input(Submit('submit', 'Submit', css_class='btn btn-outline-primary long-button', css_id='goal-button'))


class CreateGoal(forms.ModelForm):
    """ form for creating a goal object """
    class Meta:
        model = Goal
        fields = ('goal_name', 'target_date')
        widgets = {
            'target_date': DateInput()
        }

    helper = FormHelper()
    helper.add_input(Submit('submit', 'Submit', css_class='btn btn-primary'))


class CreateExercise(forms.ModelForm):
    """ form for creating an exercise object """
    class Meta:
        model = Exercise
        fields = ('exercise_name', 'type_of_exercise', 'duration', 'distance')

    helper = FormHelper()
    helper.add_input(Submit('submit', 'Submit', css_class='btn btn-primary'))


class CreateGroup(forms.ModelForm):
    """ form for creating a group object """
    class Meta:
        model = Group
        fields = ('group_name',)

    helper = FormHelper()
    helper.add_input(Submit('submit', 'Submit', css_class='btn btn-primary'))


class CreateFood(forms.ModelForm):
    """ form for creating a food object """
    class Meta:
        model = Food
        fields = ('food_name', 'calories_per_100g')

    helper = FormHelper()
    helper.add_input(Submit('submit', 'Submit', css_class='btn btn-primary'))


class CreateDrink(forms.ModelForm):
    """ form for creating a drink object """
    class Meta:
        model = Drink
        fields = ('drink_name', 'calories_per_100ml')

    helper = FormHelper()
    helper.add_input(Submit('submit', 'Submit', css_class='btn btn-primary'))


class CreateExerciseType(forms.ModelForm):
    """ form for creating a type of exercise """
    class Meta:
        model = ExerciseType
        fields = ('exercise_type_name', 'has_distance', 'has_duration')

    helper = FormHelper()
    helper.add_input(Submit('submit', 'Submit', css_class='btn btn-primary'))


class CreateWeightGoal(forms.ModelForm):
    """ form for creating a weight goal """
    class Meta:
        model = WeightGoal
        fields = ('target_weight', 'need_to_gain_weight')

    helper = FormHelper()
    helper.add_input(Submit('submit', 'Submit', css_class='btn btn-primary'))


class CreateExerciseGoal(forms.ModelForm):
    """ form for creating an exercise goal """
    class Meta:
        model = ExerciseGoal
        fields = ('exercise',)

    helper = FormHelper()
    helper.add_input(Submit('submit', 'Submit', css_class='btn btn-primary'))


class CreateMeal(forms.ModelForm):
    """ form for creating a meal object """
    class Meta:
        model = Meal
        fields = ('meal_name', 'meal_of_the_day', 'food', 'food_amount_grams', 'drink', 'drink_amount_mls', 'calories')

    helper = FormHelper()
    helper.add_input(Submit('submit', 'Submit', css_class='btn btn-primary'))
